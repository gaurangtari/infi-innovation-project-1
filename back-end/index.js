const express = require("express");
const app = express();
const mysql = require("mysql2");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  user: "root",
  host: "localhost",
  password: "12345678",
  database: "portfolio",
});

app.get("/", (req, res) => {
  res.send("Home Page");
});

app.get("/education", (req, res) => {
  db.query("SELECT * FROM education", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});
app.get("/projects", (req, res) => {
  db.query("SELECT * FROM projects", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

const port = 4000;
app.listen(port, () => {
  db.connect((err) => {
    if (err) throw err;
    console.log("database connected");
  });
  console.log("Running at port" + port);
});
