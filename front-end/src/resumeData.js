import Axios from "axios";
import { useState } from "react";

// export const education = getInfo() {
//   Axios.get("http://localhost:4000/blog-admin", {}).then((response) => {
//     return response.data;
//   });
// }

let resumeData = {
  imagebaseurl: "https://rbhatia46.github.io/",
  name: "Yatin Kanekar",
  role: "Frontend Developer and Data Scientist",
  linkedinId: "Your LinkedIn Id",
  skypeid: "Your skypeid",
  roleDescription:
    "I like dabbling in various parts of frontend development and like to learn about new technologies, write technical articles or simply play games in my free time.",
  socialLinks: [
    {
      name: "linkedin",
      url: "https://www.linkedin.com/",
      className: "fa fa-linkedin",
    },
    {
      name: "github",
      url: "http://github.com/",
      className: "fa fa-github",
    },
    {
      name: "skype",
      url: "http://twitter.com/",
      className: "fa fa-twitter",
    },
  ],
  aboutme:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  address: "India",
  website: "https://rbhatia46.github.io",
  email: "someEmail@email.com",
  education: [
    {
      UniversityName: "Goa College of Engineering",
      specialization: "BE",
      MonthOfPassing: "",
      YearOfPassing: "2010",
      Achievements: "Some Achievements",
    },
    {
      UniversityName: "MIT, Pune + ADTU, Guawahati",
      specialization: "PGDBA + MBA IT",
      MonthOfPassing: "",
      YearOfPassing: "2014",
      Achievements: "Some Achievements",
    },
    {
      UniversityName: "VSkills(Govt. Of India Initiative)",
      specialization: "Certified Selenium Professional",
      MonthOfPassing: "",
      YearOfPassing: "2015",
      Achievements: "Some Achievements",
    },
    {
      UniversityName: "MIT, Pune",
      specialization: "MBA Finance",
      MonthOfPassing: "",
      YearOfPassing: "2017",
      Achievements: "Some Achievements",
    },
    {
      UniversityName: "IIBMS, Mumbai",
      specialization: "Certificate in Strategy Mangement",
      MonthOfPassing: "",
      YearOfPassing: "2020",
      Achievements: "Some Achievements",
    },
  ],
  work: [
    {
      CompanyName: "Tech Mahindra Ltd.",
      specialization: "Technical Associate",
      Start: "Nov 2010",
      End: "July 2012",
    },
    {
      CompanyName: "Tech Mahindra Ltd.",
      specialization: "Sr. Test Engineer",
      Start: "July 2012",
      End: "June 2013",
    },
    {
      CompanyName: "HP Enterprise + Stellar",
      specialization: "Test Lead Planner",
      Start: "Dec 2015",
      End: "Till Date",
    },
  ],
  skillsDescription: "Your skills here",
  skills: [
    {
      skillname: "Telecom Billing",
    },
    {
      skillname: "Foreign Exchange",
    },
    {
      skillname: "Recycling",
    },
  ],
  portfolio: [
    {
      name: "Start-Up",
      description: "Goa",
      imgurl:
        "../public/images/291687808_403369348475548_854506102080678676_n.png",
    },
    {
      name: "Project Vijay (Reliance 4G)",
      description: "Navi Mumbai",
      imgurl: "images/portfolio/project.jpg",
    },
    {
      name: "PSI Online Migration",
      description: "Work from Home",
      imgurl: "images/portfolio/project2.png",
    },
    {
      name: "Chalo Chale",
      description: "Work from home",
      imgurl: "images/portfolio/phone.jpg",
    },
  ],
  testimonials: [
    {
      description:
        "Excellent Service and very propmt. Understands client requirement and suggest solutions accordingly. Very happy and satisfied with thier services",
      name: "Dhilan Shah",
    },
    {
      description:
        "They provide IT services across domains and technologies. A team of enthusiastic people who make sure they deliver best products as per clients requirements",
      name: "Krupesh halarnkar",
    },
  ],
};

export default resumeData;
